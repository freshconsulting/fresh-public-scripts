# vim:ft=ansible:
#
# Builds and deploys a website to a remote repository.
# The current HEAD is deployed via `git push`.
#
# Run from the git root of the repository you want to deploy.
#
# To deploy to the specified remote (e.g. wpengine-staging):
#   ansible-playbook -i localhost, ansible/deploy.yml -e "remote_name=wpengine-staging"
#
# Pushing to a remote with 'prod' in the name will tag the push as a release.
#
# This script automatically checks to ensure it is the latest version and fails if it is not.
# Run with 'is_development=YYYY-MM-DD', where YYYY-MM-DD is the current date, to override.
#
#
# To manually deploy WITHOUT this script:
# * Create a temporary branch off of the commit you would like to deploy
# * Build the site (e.g. compile css, minifiy javascript, etc)
# * Commit the build artifacts to the tempory branch
# * Delete versioned files which should not be deployed (e.g. README, composer.json, package.json, etc)
# * git push
#
#
# To set up a new project create an ansible/deploy.yml in the projct which sets
# the required variables, e.g. (uncommented):
#---
#- hosts: 127.0.0.1
#  connection: local
#  force_handlers: True
#  vars:
#    theme_name: <my theme name, e.g. fresh>
#  roles:
#    - deploy
#

---


- assert:
    that: ansible_version.major > 1
    msg: "please upgrade to ansible 2.2+"

- assert:
    that: ansible_version.minor > 1
    msg: "please upgrade to ansible 2.2+"

# setup
- name: get timestamp
  command: date +%Y-%m-%d-%H-%M
  register: timestamp
  changed_when: false

# verify inputs
- fail: msg="remote_name is required"
  when: remote_name is not defined

- fail: msg="is_development, when used, must be set to the current date in YYYY-MM-DD format"
  when: is_development and is_development != ansible_date_time.date


- name: get git root
  command: git rev-parse --show-toplevel
  register: git_root_result
  changed_when: false
  when: git_root is not defined
- set_fact:
    git_root: "{{ git_root_result.stdout }}"
  when: git_root is not defined

# verify branch is valid
- name: verify deployment remote is valid
  shell: git remote | grep "^{{ remote_name }}$"
  args:
    chdir: "{{ git_root }}"
  register: remotes
  changed_when: false
  ignore_errors: true

- fail: msg="target remote name '{{ remote_name }}' is not a valid remote for this repository"
  when: "remote_name not in remotes.stdout"

# verify file is latest
- name: set source file
  set_fact:
    file_source_url: "https://bitbucket.org/freshconsulting/fresh-public-scripts/raw/master/ansible-roles/deploy/tasks/main.yml"
  when: not is_development

- name: set md5 command - mac
  set_fact:
    md5_command: md5
  when: ansible_os_family in ["Darwin"]
- name: set md5 command - linux
  set_fact:
    md5_command: md5sum
  when: ansible_os_family in ["RedHat", "Debian"]

- name: check origin checksum
  shell: "curl -s -H 'Cache-Control: no-cache' {{ file_source_url }} | {{ md5_command }}"
  register: hash_origin
  when: not is_development
  changed_when: false

# use cat so both md5s come from stdin (md5sum includes the input filename in the output)
- name: check local checksum
  shell: cat {{ role_path }}/tasks/main.yml | {{ md5_command }}
  register: hash_local
  when: not is_development
  changed_when: false

- name: ensure script is the latest version
  fail: msg="please update this script to the latest version (this can usually be done with `cd /usr/local/fresh-public-scripts && git pull && cd -`); override with is_development=YYYY-MM-DD where YYYY-MM-DD is the current date"
  when: not is_development and hash_origin.stdout != hash_local.stdout

# check for uncomitted changes
- name: check for uncommitted changes
  shell: "! git status | grep -c modified:"
  args:
    chdir: "{{ git_root }}"
  register: uncommitted_changes
  changed_when: false
  ignore_errors: true

- fail: msg="please resolve uncomitted changes before deploying; override with is_development=YYYY-MM-DD where YYYY-MM-DD is the current date"
  when: not is_development and uncommitted_changes.failed is defined and uncommitted_changes.failed

# check for staged changes
- name: check for staged changes
  shell: "git diff --cached --numstat | wc -l"
  args:
    chdir: "{{ git_root }}"
  register: staged_changes
  changed_when: false

- fail: msg="please resolve staged uncomitted changes before deploying; otherwise these changes would be committed by this script"
  when: staged_changes.stdout|int > 0

# check for branch ahead of origin
- name: check for changes not pushed to origin
  shell: "git status | grep -c 'Your branch is up[- ]to[- ]date with'"
  args:
    chdir: "{{ git_root }}"
  register: unpushed_changes
  changed_when: false
  ignore_errors: true

- fail: msg="please push committed changes to origin before deploying; override with is_development=YYYY-MM-DD where YYYY-MM-DD is the current date"
  when: not is_development and unpushed_changes.failed is defined and unpushed_changes.failed


# check for gitignore
- name: check for gitignore
  stat: path="{{ git_root }}/.gitignore"
  register: gitignore

- fail: msg="please add a .gitignore and ensure that ignored files have not been committed"
  when: not gitignore.stat.exists

# verify branch does not already exist so we can fail without the handler running
- name: verify branch name does not already exist
  shell: git branch -a | grep " {{ release_branch_name }}$"
  args:
    chdir: "{{ git_root }}"
  register: command_result
  changed_when: false
  ignore_errors: true

- fail: msg="the branch '{{ release_branch_name }}' already exists; please delete the branch before running"
  when: "release_branch_name in command_result.stdout"

- name: get current HEAD
  # from http://stackoverflow.com/questions/6245570/how-to-get-current-branch-name-in-git
  shell: "git reflog HEAD | grep 'checkout:' | head -1 | rev | cut -d' ' -f1 | rev"
  args:
    chdir: "{{ git_root }}"
  register: git_ref
  changed_when: false

# confirm production deployment
- name: check for production deployment
  set_fact:
    is_production_deployment: "{{ 'prod' in remote_name }}"

- name: confirm production deployment
  pause: prompt="WARNING deploying to a production remote. Press enter to continue or Ctrl-C and then A to cancel."
  when: is_production_deployment | bool

# check for README
- name: check for README.md
  stat: path="{{ git_root }}/README.md"
  register: readme_stat
- fail: msg="[WARNING] No README.md found. Please add a README.md file explaining this project purpose and setup instructions."
  when: not readme_stat.stat.exists
  ignore_errors: true

# create a branch
- name: create a temporary release branch
  command: git checkout -b {{ release_branch_name }}
  args:
    chdir: "{{ git_root }}"
  notify:
    - cleanup deployment branch



# list build directories

- name: initialize variables
  set_fact:
    build_dirs: [ "{{ git_root }}" ]


# where should we check for builds files?
- name: get default WordPress theme dirs
  find:
    paths: "{{ git_root }}/wp-content/themes"
    file_type: directory
    recurse: no
  register: default_wordpress_theme_dirs
- name: add directories to list 1
  set_fact:
    build_dirs: "{{ build_dirs + [item.path] }}"
  with_items: "{{ default_wordpress_theme_dirs['files'] }}"

- name: get sage 9 WordPress theme directories
  find:
    paths: "{{ git_root }}/web/app/themes"
    file_type: directory
    recurse: no
  register: sage9_wordpress_theme_dirs
- name: add directories to list 2
  set_fact:
    build_dirs: "{{ build_dirs + [item.path] }}"
  with_items: "{{ sage9_wordpress_theme_dirs['files'] }}"


- debug: var=build_dirs
  notify:
    - rebuild
  changed_when: true


- name: build the directories
  include_tasks: build.yml
  vars:
    build_root: "{{ item }}"
    commit_changes: 1
  with_items: "{{ build_dirs }}"



# delete private files
- name: stash changes
  command: git stash save
  args:
    chdir: "{{ git_root }}"
  when: uncommitted_changes.failed is defined and uncommitted_changes.failed
  notify: cleanup stash
- name: delete private files
  command: git rm --cached --ignore-unmatch {{ item }}
  with_items:
    - "*.sublime-project"
    - bitbucket-pipelines.yml
    - bower.json
    - composer.json
    - composer.lock
    - Gruntfile.js
    - gulpfile.js
    - package.json
    - package-lock.json
    - readme.html
    - readme.md
    - readme.mkd
    - readme.txt
    - README.md
    - README.mkd
    - README.txt
    - Vagrantfile
    - wp-content/themes/*/bower.json
    - wp-content/themes/*/composer.lock
    - wp-content/themes/*/Gruntfile.js
    - wp-content/themes/*/gulpfile.js
    - wp-content/themes/*/package-lock.json
    - wp-content/themes/*/package.json
    - wp-content/themes/*/yarn.lock
    - wp-content/themes/*/vendor/symfony/console/Resources/bin/hiddeninput.exe
    - wp-content/plugins/hello.php
  args:
    chdir: "{{ git_root }}"
- name: check for staged changes
  shell: git diff --cached --numstat | wc -l
  args:
    chdir: "{{ git_root }}"
  register: deleted_changes
  changed_when: false
- name: commit delete changes
  command: git commit -m 'delete private files'
  args:
    chdir: "{{ git_root }}"
  when: deleted_changes.stdout|int > 0

- name: check for wpengine remote
  set_fact:
    is_wpengine: "{{ 'wpengine' in remote_name }}"

- name: set push command
  set_fact:
    push_command: git push --force  {{ remote_name }} {{ release_branch_name }}

# deploy the codes
- name: deploy the codes
  command: "{{ push_command }}"
  args:
    chdir: "{{ git_root }}"
  register: deployment
- name: deployment output
  debug: var="deployment.stderr.split('\n')"

# tag the release
- name: create git release tag
  command: "{{ item }}"
  with_items:
    - git tag release_src_{{ timestamp.stdout }} {{ git_ref.stdout }}
    - git push --tags
  args:
    chdir: "{{ git_root }}"
  when: is_production_deployment | bool
