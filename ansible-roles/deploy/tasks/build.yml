# vim:ft=ansible:
#
# Builds a website.
#
# Does not install dependencies (e.g. `npm install`, `bower install`, etc). You need to do that yourself.
#
# Called from the `deploy` playbook.
#

---


# verify inputs
- fail: msg="build_root is required"
  when: build_root is not defined

- debug: msg="***** running build in directory {{ build_root }} *****"

# verify build_root is valid
- name: verify build directory is valid
  stat: path="{{ build_root }}"
  register: build_root_stat

- fail: msg="build directory {{ build_root }} not found"
  when: not build_root_stat.stat.exists

- set_fact:
    directory_name: "{{ build_root.split('/')[-1] }}"


# build
- name: check for gruntfile
  stat: path={{ build_root }}/Gruntfile.js
  register: gruntfile_js
- name: check for gulpfile
  stat: path={{ build_root }}/gulpfile.js
  register: gulpfile_js
- name: check for yarn
  stat: path={{ build_root }}/yarn.lock
  register: yarn_lock
- name: check for sage10
  stat: path={{build_root}}/jsconfig.json
  register: jsconfig_json
- name: check for build_root composer
  stat: path={{ build_root }}/composer.json
  register: build_root_composer_json


- name: set grunt command and paths
  set_fact:
    build_command: grunt build --no-color
    build_artifact_paths:
      - "{{ build_root }}/css/*.css"
      - "{{ build_root }}/css/*.map"
      - "{{ build_root }}/assets/css/*.css"
      - "{{ build_root }}/assets/css/*.map"
      - "{{ build_root }}/assets/js/*.min.js"
      - "{{ build_root }}/assets/manifest.json"
      - "{{ build_root }}/lib/scripts.php"
  when: gruntfile_js.stat.exists
- name: set gulp command and paths
  set_fact:
    build_command: gulp --production --no-color
    build_artifact_paths:
      - "{{ build_root }}/dist"
  when: gulpfile_js.stat.exists
- name: set npm script command and paths
  set_fact:
    build_command: yarn run build:production
    build_artifact_paths:
      - "{{ build_root }}/dist"
  when: yarn_lock.stat.exists and not jsconfig_json.stat.exists

- name: set npm script command and paths for sage10
  set_fact:
    build_command: yarn build --clean
    build_artifact_paths:
      - "{{ build_root }}/public"
  when: jsconfig_json.stat.exists

- name: build release artifacts
  command: "{{ build_command }}"
  register: build
  args:
    chdir: "{{ build_root }}"
  when: gruntfile_js.stat.exists or gulpfile_js.stat.exists or yarn_lock.stat.exists
- name: build output
  debug: var="build.stdout.split('\n')"
  when: gruntfile_js.stat.exists or gulpfile_js.stat.exists or yarn_lock.stat.exists

- name: build_root composer install
  command: composer install --no-dev --optimize-autoloader 
  args:
    chdir: "{{ build_root }}"
  when: build_root_composer_json.stat.exists

# add build files
- name: add build artifacts
  command: git add --force {{ path }}
  with_items: "{{ build_artifact_paths }}"
  loop_control:
    loop_var: path
  args:
    chdir: "{{ git_root }}"
  when: commit_changes and (gruntfile_js.stat.exists or gulpfile_js.stat.exists or yarn_lock.stat.exists)
  register: build_artifacts_added
  ignore_errors: true

- name: commit build changes
  command: git commit -m 'add `{{ build_command }}` artifacts (dir{{':'}} {{ directory_name }})'
  args:
    chdir: "{{ git_root }}"
  when: build_artifacts_added.changed and not (build_artifacts_added.failed is defined and build_artifacts_added.failed)

- name: build_root add composer files
  command: git add --force vendor
  args:
    chdir: "{{ build_root }}"
  when: commit_changes and build_root_composer_json.stat.exists
  register: composer_files_added
  ignore_errors: true

- name: commit composer changes
  command: git commit -m 'add composer files (dir{{':'}} {{ directory_name }})'
  args:
    chdir: "{{ git_root }}"
  when: composer_files_added.changed and not (composer_files_added.failed is defined and composer_files_added.failed)


