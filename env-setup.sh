#!/bin/sh
#
# Installs the minimum requirements for a development environment
# run with:
#
#   curl -s https://bitbucket.org/freshconsulting/fresh-public-scripts/raw/master/env-setup.sh|bash
#
#

INSTALL_DIR=/usr/local
REPO_DIR=${INSTALL_DIR}/fresh-public-scripts
DESIRED_ROLES_PATH=${REPO_DIR}/ansible-roles
WPCS_DIR=${INSTALL_DIR}/phpcs/wpcs
WCAG_DIR=${INSTALL_DIR}/phpcs/wcag

for DIR in ${REPO_DIR} ${WPCS_DIR} ${WCAG_DIR}
do
    if [ -w ${DIR} ]; then
      echo "Write permission to ${DIR} verified"
    else
      echo "You cannot write to ${DIR}. Fixing...."
      sudo mkdir -p ${DIR}
      sudo chown ${USER}:admin ${DIR}
    fi
done


if xcode-select -p &> /dev/null; then
    echo "Developer Tools located"
else
    echo "Developer Tools not installed; let's do that now"
    echo "Please click 'Install' on the popup and follow the prompts"
    xcode-select --install
    echo "Successfully installed Develper Tools"
fi


if /usr/bin/xcrun clang 2>&1 | grep license &> /dev/null; then
    echo "You are agreeing to the XCode Developer Tools License (fyi)"
    sudo xcodebuild -license accept
    echo "License agreement registered"
else
    echo "Developer Tools license ok"
fi


if type brew &> /dev/null; then
  echo "Awesome! Homebrew is installed already!"
else
  echo "Homebrew is not installed; let's do that now"
  echo "Installing Homebrew"
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  echo "Successfully installed Homebrew"
fi


if type git &> /dev/null; then
  echo "Spectacular! Git is installed already!"
else
  echo "Git is not installed; let's do that now"
  echo "Installing Git "
  brew install git
  echo "Successfully installed Git"
fi


if type ansible &> /dev/null; then
  echo "Bedazzling! Ansible is installed already!"
else
  echo "Ansible is not installed; let's do that now"
  echo "Installing Ansible "
  brew install ansible
  echo "Successfully installed Ansible"
fi


if [ ! -d ${REPO_DIR}/.git ]; then
  echo "Cloning into fresh-public-scripts"
  git clone https://bitbucket.org/freshconsulting/fresh-public-scripts ${REPO_DIR}
else
  echo "Great! fresh-public-scripts is installed already!"
  echo "Updating..."
  cd ${REPO_DIR} && git pull
fi

#if type phpcs &> /dev/null; then
#  echo "Superb! phpcs is installed already!"
#else
#  echo "phpcs is not installed; let's do that now"
#  echo "Installing phpcs"
#  brew install php-code-sniffer
#  echo "Successfully installed VirtualBox"
#fi

if [ ! -d ${WPCS_DIR}/.git ]; then
  echo "Cloning into WordPress-Coding-Standards"
  mkdir -p ${WPCS_DIR}
  git clone -b main https://github.com/WordPress/WordPress-Coding-Standards.git ${WPCS_DIR}
else
  echo "Outstanding! WordPress Coding Standards is installed already!"
  echo "Updating..."
  cd ${WPCS_DIR} && git pull
fi

if [ ! -d ${WCAG_DIR}/.git ]; then
  echo "Cloning into FreshConsulting-WCAG-Linter"
  mkdir -p ${WCAG_DIR}
  git clone -b master https://bitbucket.org/freshconsulting/freshconsulting-wcag-linter.git ${WCAG_DIR}
else
  echo "Woo! FreshConsulting WCAG Linter is installed already!"
  echo "Updating..."
  cd ${WCAG_DIR} && git pull
fi

#phpcs --config-set installed_paths ${WPCS_DIR},${WCAG_DIR}

if [ -f ~/.ansible.cfg ]; then
  echo "Excellent! You already have the ansible configuration file"
  CURRENT_ROLES_PATH=`cat ~/.ansible.cfg | grep roles_path | cut -d'=' -f 2 | tr -d ' '`
  if [ ${CURRENT_ROLES_PATH} = ${DESIRED_ROLES_PATH} ]; then
      echo "OK! roles_path looks good"
  else
      echo "Action required! Change your Ansible roles_path to ${DESIRED_ROLES_PATH}"
  fi
else
  cat > ~/.ansible.cfg << END_TEXT
[ssh_connection]
scp_if_ssh = True
[defaults]
retry_files_enabled = False # Do not create them
roles_path = ${DESIRED_ROLES_PATH}
END_TEXT
  echo "Ansible configuration file created"
fi


