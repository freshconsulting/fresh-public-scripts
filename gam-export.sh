#!/bin/sh

# GAM is https://github.com/taers232c/GAMADV-XTD3

# update this to match your GAM install
FULLY_QUALIFIED_GAM=/Users/hulet/bin/gamadv-xtd3/gam
OUTPUT_DIR=/tmp/gam/

mkdir -p ${OUTPUT_DIR}

# domaincontact is an alias of peoplecontact
# user is an alias of users
# org is an alias of orgs
COMMANDS=(
    addresses
    admin
    adminrole
    alias
    browser
    building
    chromeapp
    chromeaues
    chromeneedsattn
    chromesnvalidity
    chromeversions
    contact
    cros
    datatransfer
    domain
    domainalias
    drivelabel
    feature
    group
    groupmembers
    inboundssoassignment
    inboundssocredential
    inboundssoprofile
    license
    mobile
    orgs
    orgunitshareddrive
    peoplecontact
    privileges
    resources
    schema
    shareddrive
    shareddriveacls
    token
    transferapps
    userinvitation
    users
    vaultcount
    vaultexport
    vaulthold
    vaultmatter
    vaultquery
)


for i in ${COMMANDS[*]}
do
    echo "========== $i =========="
    ${FULLY_QUALIFIED_GAM} print ${i} > ${OUTPUT_DIR}/${i}
done
