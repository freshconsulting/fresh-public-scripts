``` Shell
sudo mkdir /usr/local/fresh-public-scripts
sudo chown ${USER}:staff /usr/local/fresh-public-scripts
git clone git@bitbucket.org:freshconsulting/fresh-public-scripts.git /usr/local/fresh-public-scripts
/usr/local/fresh-public-scripts/env-setup.sh
```
